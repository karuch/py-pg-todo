FROM python:3.12.3-alpine3.19

RUN python3 -m venv .venv

COPY . .venv

WORKDIR .venv

RUN pip install -r ./requirements.txt

ENTRYPOINT ["python3"]

CMD ["app.py"]

